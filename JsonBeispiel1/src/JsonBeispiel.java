import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class JsonBeispiel 
{

	public static void main(String[] args) 
	{
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", "XQuery Kick Start");
		
		JsonObjectBuilder verlagBuilder = Json.createObjectBuilder();
		verlagBuilder.add("name", "Sams");
		verlagBuilder.add("ort", "Indianapolis");
		bookBuilder.add("verlag", verlagBuilder);
		
		JsonArrayBuilder autorListeBuilder = Json.createArrayBuilder();
		autorListeBuilder.add("Autor1");
		autorListeBuilder.add("Autor2");
		autorListeBuilder.add("Autor3");
		bookBuilder.add("autoren", autorListeBuilder);
		
		JsonObject jo = bookBuilder.build();
		
		try 
		{
			FileWriter fw = new FileWriter("book.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		

	}
}
