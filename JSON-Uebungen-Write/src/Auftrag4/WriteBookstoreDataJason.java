package Auftrag4;

import java.io.FileWriter;

/* Auftrag 4) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung4.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Straße",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	},
	"buchliste": [
		{
			"titel": "Java ist auch eine Insel",
			"jahr": 1998,
			"preis": 49.9,
			"autor": "Christian Ullenboom"
		},
		{
			"titel": "SQL: Handbuch für Einsteiger",
			"jahr": 2020,
			"preis": 17.99,
			"autor": "Paul Fuchs"
		},
		{
			"titel": "PHP und MySQL für einsteiger",
			"jahr": 2018,
			"preis": 9.99,
			"autor": "Michael Bonacina"
		},
		{
			"titel": "Einführung in SQL",
			"jahr": 2021,
			"preis": 29.9,
			"autor": "Alan Beaulieu"
		},
		{
			"titel": "Java Programmieren für Einsteiger",
			"jahr": 2020,
			"preis": 13.95,
			"autor": "Simon Flaig"
		},
		{
			"titel": "Git Handbuch für Einsteiger",
			"jahr": 2021,
			"preis": 19.99,
			"autor": "Paul Fuchs"
		},
		{
			"titel": "Captain CiaoCiao erobert Java",
			"jahr": 2021,
			"preis": 39.9,
			"autor": "Paul Fuchs"
		},
		{
			"titel": "Java: Kompendium: Professionell Java programmieren lernen",
			"jahr": 2019,
			"preis": 19.99,
			"autor": "Markus Neumann"
		},
		{
			"titel": "Python Crash Course, 2nd Edition",
			"jahr": 2019,
			"preis": 25.99,
			"autor": "Eric Matthes"
		}
	]
}
*/

import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

public class WriteBookstoreDataJason {

	public static void main(String[] args) {

		ArrayList<Buch> buchliste = new ArrayList<Buch>();
		buchliste.add(new Buch("Java ist auch eine Insel", 1998, 49.90, "Christian Ullenboom"));
		buchliste.add(new Buch("SQL: Handbuch für Einsteiger", 2020, 17.99, "Paul Fuchs"));
		buchliste.add(new Buch("PHP und MySQL für einsteiger", 2018, 9.99, "Michael Bonacina"));
		buchliste.add(new Buch("Einführung in SQL", 2021, 29.90, "Alan Beaulieu"));
		buchliste.add(new Buch("Java Programmieren für Einsteiger", 2020, 13.95, "Simon Flaig"));
		buchliste.add(new Buch("Git Handbuch für Einsteiger", 2021, 19.99, "Paul Fuchs"));
		buchliste.add(new Buch("Captain CiaoCiao erobert Java", 2021, 39.90, "Paul Fuchs"));
		buchliste.add(new Buch("Java: Kompendium: Professionell Java programmieren lernen", 2019, 19.99, "Markus Neumann"));
		buchliste.add(new Buch("Python Crash Course, 2nd Edition", 2019, 25.99, "Eric Matthes"));

		// TODO add your code here
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("name", "OSZIMT Buchhandlung");
		bookBuilder.add("tel", "030-225027-800");
		bookBuilder.add("fax", "030-225027-809");

		JsonObjectBuilder adresse = Json.createObjectBuilder();
		adresse.add("stasse", "haarlemer Strasse");
		adresse.add("hausnummer", "23-27");
		adresse.add("plz", "12359");
		adresse.add("ort", "Berlin");
		bookBuilder.add("strasse", adresse);

		
		
		JsonObjectBuilder javaIstAuchEineInsel = Json.createObjectBuilder();
		javaIstAuchEineInsel.add("titel", "Java ist auch eine Insel");
		javaIstAuchEineInsel.add("jahr", "1998");
		javaIstAuchEineInsel.add("preis", "49.90");
		javaIstAuchEineInsel.add("autor", "Christian Ullenboom");
		
		JsonObjectBuilder SQLHandbuch = Json.createObjectBuilder();
		SQLHandbuch.add("titel", "SQL: Handbuch für Einsteiger");
		SQLHandbuch.add("jahr", "2020");
		SQLHandbuch.add("preis", "19.99");
		SQLHandbuch.add("autor", "Paul Fuchs");
		
		JsonObjectBuilder PHPundMySQLfüreinsteiger = Json.createObjectBuilder();
		PHPundMySQLfüreinsteiger.add("titel", "PHP und My SQL für einsteiger");
		PHPundMySQLfüreinsteiger.add("jahr", "2018");
		PHPundMySQLfüreinsteiger.add("preis", "9.99");
		PHPundMySQLfüreinsteiger.add("autor", "Michael Bonacina");
		
		JsonObjectBuilder EinführunginSQL = Json.createObjectBuilder();
		EinführunginSQL.add("titel", "Einführung in SQL");
		EinführunginSQL.add("jahr", "2021");
		EinführunginSQL.add("preis", "29.90");
		EinführunginSQL.add("autor", "Alan Beaulieu");
		
		JsonObjectBuilder JavaProgrammierenfürEinsteiger = Json.createObjectBuilder();
		JavaProgrammierenfürEinsteiger.add("titel", "Java Programmieren für Einsteiger");
		JavaProgrammierenfürEinsteiger.add("jahr", "2020");
		JavaProgrammierenfürEinsteiger.add("preis", "13.95");
		JavaProgrammierenfürEinsteiger.add("autor", "Simon Flaig");
		
		JsonObjectBuilder GitHandbuchfürEinsteiger = Json.createObjectBuilder();
		GitHandbuchfürEinsteiger.add("titel", "Git Handbuch für Einsteiger");
		GitHandbuchfürEinsteiger.add("jahr", "2021");
		GitHandbuchfürEinsteiger.add("preis", "19.99");
		GitHandbuchfürEinsteiger.add("autor", "Paul Fuchs");
		
		JsonObjectBuilder CaptainCiaoCiaoerobertJava = Json.createObjectBuilder();
		CaptainCiaoCiaoerobertJava.add("titel", "Captain Ciao erobert Java");
		CaptainCiaoCiaoerobertJava.add("jahr", "2021");
		CaptainCiaoCiaoerobertJava.add("preis", "39.99");
		CaptainCiaoCiaoerobertJava.add("autor", "Paul Fuchs");
		
		JsonObjectBuilder JavaKompendiumProfessionellJavaprogrammierenlernen = Json.createObjectBuilder();
		JavaKompendiumProfessionellJavaprogrammierenlernen.add("titel", "Java: Kompendium: Professionell Java programmieren lernen");
		JavaKompendiumProfessionellJavaprogrammierenlernen.add("jahr", "2019");
		JavaKompendiumProfessionellJavaprogrammierenlernen.add("preis", "19.99");
		JavaKompendiumProfessionellJavaprogrammierenlernen.add("autor", "Markus Neumann");
		
		JsonObjectBuilder PythonCrashCourse2ndEdition = Json.createObjectBuilder();
		PythonCrashCourse2ndEdition.add("titel", "Python Crash Course, 2nd Edition");
		PythonCrashCourse2ndEdition.add("jahr", "2019");
		PythonCrashCourse2ndEdition.add("preis", "25.99");
		PythonCrashCourse2ndEdition.add("autor", "Eric Matthes");
		
		JsonArrayBuilder autorListeBuilder = Json.createArrayBuilder();
		autorListeBuilder.add(javaIstAuchEineInsel);
		autorListeBuilder.add(SQLHandbuch);
		autorListeBuilder.add(PHPundMySQLfüreinsteiger);
		autorListeBuilder.add(EinführunginSQL);
		autorListeBuilder.add(JavaProgrammierenfürEinsteiger);
		autorListeBuilder.add(GitHandbuchfürEinsteiger);
		autorListeBuilder.add(CaptainCiaoCiaoerobertJava);
		autorListeBuilder.add(JavaKompendiumProfessionellJavaprogrammierenlernen);
		autorListeBuilder.add(PythonCrashCourse2ndEdition);
		
		bookBuilder.add("Buchliste", autorListeBuilder);
		JsonObject jo = bookBuilder.build();

		try {
			FileWriter fw = new FileWriter("book4.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
