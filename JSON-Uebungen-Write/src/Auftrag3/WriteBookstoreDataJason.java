package Auftrag3;

import java.io.FileWriter;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

/* Auftrag 3) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung3.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Strasse",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	}
}

*/

public class WriteBookstoreDataJason {

	public static void main(String[] args) {

		// TODO add your code here
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("name", "OSZIMT Buchhandlung");
		bookBuilder.add("tel", "030-225027-800");
		bookBuilder.add("fax", "030-225027-809");
		
		JsonObjectBuilder adresse = Json.createObjectBuilder();
		adresse.add("stasse", "haarlemer Strasse");
		adresse.add("hausnummer", "23-27");
		adresse.add("plz", "12359");
		adresse.add("ort", "Berlin");
		bookBuilder.add("strasse", adresse);
		
		JsonObject jo = bookBuilder.build();

		try {
			FileWriter fw = new FileWriter("book3.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
