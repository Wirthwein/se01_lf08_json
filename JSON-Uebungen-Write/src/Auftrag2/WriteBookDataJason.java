package Auftrag2;

import java.io.FileWriter;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

/* Auftrag 2) Erstellen Sie folgende book2.json:
{
	"titel": "Java ist auch eine Insel",
	"jahr": 1998,
	"preis": 29.95,
	"autor": "Christian Ullenboom"
}
*/
public class WriteBookDataJason {

	public static void main(String[] args) {

		Buch b1 = new Buch("Java ist auch eine Insel", 1998, 29.95, "Christian Ullenboom");
		

		// TODO add you code here
		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", "Java ist auch eine Insel");
		bookBuilder.add("Jahr", "1998");
		bookBuilder.add("preis", "29.95");
		bookBuilder.add("autor", "Christian Ullenboom");
		
		JsonObject jo = bookBuilder.build();
		
		try 
		{
			FileWriter fw = new FileWriter("book.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
