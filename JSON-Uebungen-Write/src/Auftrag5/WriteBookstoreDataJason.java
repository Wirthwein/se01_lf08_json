package Auftrag5;

/* Auftrag 5) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung5.json:
{
	"name": "OSZIMT Buchhandlung",
	"tel": "030-225027-800",
	"fax": "030-225027-809",
	"adresse": {
		"strasse": "Haarlemer Straße",
		"hausnummer": "23-27",
		"plz": "12359",
		"ort": "Berlin"
	},
	"buchliste": [
		{
			"titel": "Java ist auch eine Insel",
			"jahr": 1998,
			"preis": 49.9,
			"Autorenliste": [
				"Christian Ullenboom",
				"Paul Fuchs"
			]
		},
		{
			"titel": "XQuery Kick Start",
			"jahr": 2020,
			"preis": 17.99,
			"Autorenliste": [
				"James McGovern",
				"Per Bothner",
				"Kurt Cagle",
				"James Linn"
			]
		}
	]
}
*/

import java.util.ArrayList;

public class WriteBookstoreDataJason {

	public static void main(String[] args) {

		ArrayList<Buch> buchliste = new ArrayList<Buch>();
		ArrayList<String> autorenliste = new ArrayList<String>();

		autorenliste.add("Christian Ullenboom");
		autorenliste.add("Paul Fuchs");
		buchliste.add(new Buch("Java ist auch eine Insel", 1998, 49.90, autorenliste));

		autorenliste = new ArrayList<String>();
		autorenliste.add("James McGovern");
		autorenliste.add("Per Bothner");
		autorenliste.add("Kurt Cagle");
		autorenliste.add("James Linn");
		buchliste.add(new Buch("XQuery Kick Start", 2020, 17.99, autorenliste));

		// TODO add your code here

	}

}
